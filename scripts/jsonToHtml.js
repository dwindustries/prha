const data = {
  existing_resident: "I am not an existing resident",
  personal_household_title: "Mrs",
  personal_household_first_name: "Quinlan",
  personal_household_surname: "Burt",
  personal_household_relationship: "Father",
  personal_household_ni_number: 533,
  household_others_first_title: "Mr",
  household_others_first_first_name: "Caryn",
  household_others_first_surname: "Wall",
  household_others_first_relationship: "Mother",
  household_others_first_ni_number: 530,
  household_others_second_title: "Mr",
  household_others_second_first_name: "Cassady",
  household_others_second_surname: "Horn",
  household_others_second_relationship: "Brother",
  household_others_second_ni_number: 986,
  current_address_line1: "903 East Old Extension",
  current_address_line2: "Qui harum consequatu",
  current_address_postcode: "E107TT",
  details_telephone: 15611165165,
  details_mobile: 515611651,
  details_email: "tiliwolud@mailinator.com",
  details_email_confirm: "tiliwolud@mailinator.com",
  details_nomination: "Quas et cupidatat co",
  first_preferred_contact_title: "Mrs",
  first_preferred_contact_first_name: "Cadman",
  first_preferred_contact_surname: "Parrish",
  first_preferred_contact_email: "xizov@mailinator.com",
  first_preferred_contact_relationship: "Quasi culpa velit do",
  first_preferred_contact_address_line1: "432 South Cowley Road",
  first_preferred_contact_address_line2: "Nihil id sunt incid",
  first_preferred_contact_postcode: "E107TT",
  second_preferred_contact_title: "Ms",
  second_preferred_contact_first_name: "Paul",
  second_preferred_contact_surname: "Ingram",
  second_preferred_contact_email: "qenef@mailinator.com",
  second_preferred_contact_relationship: "Father",
  second_preferred_contact_address_line1: "89 Nobel Lane",
  second_preferred_contact_address_line2: "Aut ipsa ea volupta",
  second_preferred_contact_postcode: "E107TT",
  current_housing_reasons: "Sit cupiditate cupi",
  current_housing_current_type: "Private tenant",
  current_housing_current_type_other_specify: "Natus necessitatibus",
  current_housing_current_sheltered: "Not Sheltered Accommodation",
  current_housing_current_property: "Room in shared House",
  current_housing_current_property_other_specify: "Dolorum non omnis Na",
  current_housing_current_property_flat_specify: "Ullamco et consequat",
  current_housing_current_property_do_you_have_to_climb_stairs: "",
  previous_resident: "I am a previous PRHA resident",
  previous_resident_address: "Esse non veniam re",
  pet: "I do have a pet",
  pet_details: "Modi distinctio Sin",
  housing_benefit: "I do claim Housing Benefit",
  housing_benefit_details: "Sed aliqua Eum aliq",
  eligibility_right_of_residence:
    "Not everyone to be housed with me has the right of residence in the UK",
  eligibility_right_of_residence_details: "Omnis quibusdam duis",
  eligibility_prha_related:
    "Yes someone housed with me is related to a PRHA member of staff or a member of its Board",
  eligibility_prha_related_details: "Iure delectus ducim",
  current_property_length_years: 3,
  current_property_length_months: 2,
  current_property_length_address: "Qui maxime in ullamc",
  landlord_address: "Ut incidunt et rati",
  arrears: "Yes I owe rent arrears",
  arrears_clearing: "Yes I have made arrangements to clear these arrears",
  arrears_clearing_details: "Est ex minus volupt",
  evicted: "Yes someone in my household has been evicted",
  asbo: "No - ASBO",
  asbo_details: "Quis quis doloribus",
  convictions: "No one in my household has convictions",
  convictions_details: "Dolorum consequuntur",
  probation_worker: "No one in my household has a Probation Worker",
  probation_worker_details: "Sed cillum nihil con",
  power_of_attorney:
    "No I do not have a person or persons acting for me under a Power of Attorney?",
  power_of_attorney_details: "Odio neque sapiente",
  where_did_you_hear_about_prha: "Age UK",
  where_did_you_hear_about_prother_specify: "Est facilis labore",
  accommodation_required_type: "Bungalow",
  accommodation_required_floor_level: "Ground",
  accommodation_required_property_type: "1 Bedroom,2 Bedroom",
  accommodation_required_large_accommodation_details: "Sed laboris harum no",
  accommodation_required_scheme_area_one: "Laboriosam molestia",
  accommodation_required_scheme_name_one: "Dacey Woodard",
  accommodation_required_scheme_area_two: "Dignissimos lorem ve",
  accommodation_required_scheme_name_two: "Solomon Turner",
  accommodation_required_scheme_area_three: "Error et officiis fu",
  accommodation_required_scheme_name_three: "Maggie Copeland",
  accommodation_required_scheme_area_four: "Ut culpa adipisicing",
  accommodation_required_scheme_name_four: "Aimee Carney",
  currently_working: "Yes currently working",
  current_working_hours: 20,
  mobility_current: "Expedita qui amet r",
  mobility_get_about: "Dolorem et amet sin",
  mobility_falls: "Quam reprehenderit",
  mobility_smoke_alcohol: "Eiusmod consequuntur",
  mobility_climb_stairs: "Consequuntur sed sed",
  mobility_difficult_tasks: "Possimus optio asp",
  mobility_scooter: "Harum facilis in ut",
  mobility_doctor_details: "Qui natus perspiciat",
  declaration_full_name: "Uta Salazar",
  declaration_joint_full_name: "Amena Cohen",
  ip: "217.155.35.5",
  user_agent:
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
  referrer: "https://prha.co.uk/housing-application/",
  created_at: "2021-07-08T15:35:27.511Z",
}

function replaceAllChars(str) {
  return str.replace(/_/g, " ")
}

function titleCase(str) {
  return str
    .toLowerCase()
    .split(" ")
    .map((word) => {
      const titledWord = word.charAt(0).toUpperCase() + word.slice(1)
      return replaceAllChars(titledWord)
    })
    .join(" ")
}

function outputHTML() {
  // take the data in json file
  // loop over the keys
  // and log each item as html string
  const keys = Object.keys(data)
  keys.map((item) => {
    // capitalise and add space
    console.log(`<p><b>${titleCase(item)}</b>: {{${item}}}</p>`)
  })
}

outputHTML()
