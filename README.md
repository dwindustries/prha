<h1 align="center">
  PRHA
</h1>

Main site for Portsmouth Rotary Housgin Association.

Frontend uses Gatbsy.js project using React pulling in data via GraphQL, hosted on Netlify.
Backend uses Wordpress using WP REST API, admin panel at https://production.prha.co.uk/wp-admin

To launch frontend locally use `gatsby develop`
Currently uses Node v8.11.1 (so might need to run nvm use v8 before launch)