const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_POSTS = `
    query GET_POSTS($limit:Int, $skip:Int){
      allWpPost(
        limit: $limit 
        skip: $skip
        sort: { order: ASC, fields: menuOrder }
      ) {
        nodes {
          id
          slug
          title
          featuredImage {
            node {
              altText
              localFile {
                childImageSharp {
                  gatsbyImageData
                }
              }
            }
          }
        }
      }
    }`

  const { createPage } = actions
  const allPosts = []
  const fetchPosts = async variables =>
    await graphql(GET_POSTS, variables).then(({ data }) => {
      const {
        allWpPost: { nodes },
      } = data

      nodes.map(post => {
        allPosts.push(post)
      })
      return allPosts
    })

  await fetchPosts({ limit: 100 }).then(allPosts => {
    // Create news list pages
    const postsPerPage = 9
    const numPages = Math.ceil(allPosts.length / postsPerPage)

    Array.from({ length: numPages }).forEach((_, i) => {
      console.log(`createBlogPage ${i}`)
      createPage({
        path: i === 0 ? `/about-us/news` : `/about-us/news/${i + 1}`,
        component: path.resolve("./src/templates/news.js"),
        context: {
          limit: postsPerPage,
          skip: i * postsPerPage,
          numPages,
          currentPage: i + 1,
          allPosts,
        },
      })
    })

    // Create single post pages
    const postTemplate = path.resolve(`./src/templates/post.js`)
    allPosts.map(post => {
      console.log(`create post: ${post.slug}`)
      createPage({
        path: `/${post.slug}/`,
        component: postTemplate,
        context: post,
      })
    })
  })
}
