const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_PAGES = `
    query GET_PAGES($limit:Int, $skip:Int){
      allWpPage(
        limit: $limit 
        skip: $skip     
      ) {
        pageInfo {
          itemCount
          hasNextPage
        }
        nodes {
          id
          slug
          uri
          title
          pageTemplate
        }
      }
    }
  `
  const { createPage } = actions
  const allPages = []
  const fetchPages = async variables =>
    await graphql(GET_PAGES, variables).then(({ data }) => {
      const {
        allWpPage: {
          pageInfo: { hasNextPage, itemCount },
          nodes,
        },
      } = data
      nodes.map(page => {
        allPages.push(page)
      })
      if (hasNextPage) {
        return fetchPages({ limit: variables.limit, skip: itemCount })
      }
      return allPages
    })

  await fetchPages({ limit: 100, skip: null }).then(allPages => {
    const templates = {
      default: path.resolve(`./src/templates/page.js`),
      home: path.resolve(`./src/templates/home.js`),
      parent: path.resolve(`./src/templates/parent.js`),
      child: path.resolve(`./src/templates/child.js`),
      downloads: path.resolve(`./src/templates/downloads.js`),
      staff: path.resolve(`./src/templates/staff.js`),
      properties: path.resolve(`./src/templates/properties.js`),
      housingApplication: path.resolve(`./src/templates/housingApplication.js`),
    }

    let pageTemplate = templates.default
    allPages.map(page => {
      switch (page.pageTemplate) {
        case "templates/home.php":
          pageTemplate = templates.home
          break
        case "templates/parent.php":
          pageTemplate = templates.parent
          break
        case "templates/child.php":
          pageTemplate = templates.child
          break
        case "templates/downloads.php":
          pageTemplate = templates.downloads
          break
        case "templates/staff.php":
          pageTemplate = templates.staff
          break
        case "templates/properties.php":
          pageTemplate = templates.properties
          break
        case "templates/housing-application.php":
          pageTemplate = templates.housingApplication
          break
        default:
          pageTemplate = templates.default
          break
      }

      console.log(`create page: ${page.slug}`)
      createPage({
        path: page.uri,
        component: pageTemplate,
        context: page,
      })
    })
  })
}
