// gatsby-config.js
module.exports = {
  siteMetadata: {
    title: `Portsmouth Rotary Housing Association`,
    description: `Portsmouth Rotary Housing Association website`,
    author: `https://dan-webb.com`,
    // image: `${__dirname}/src/images/social-feature-image.jpg`,
    wordPressUrl: `https://production.prha.co.uk`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `PRHA`,
        short_name: `prha`,
        start_url: `/`,
        background_color: `#154D91`,
        theme_color: `#F4A800`,
        icon: "src/images/favicon.png", // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        /*
         * The full URL of the WordPress site's GraphQL API.
         */
        url: `https://production.prha.co.uk/graphql`,
      },
    },
    `gatsby-plugin-sass`,
    // Create restricted routes for our members area (via Firebase login)
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/members/*`] },
    },
  ],
}
