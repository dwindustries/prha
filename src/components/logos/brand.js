import React from 'react'
import logo from "../../images/prha.svg"
import { Link } from 'gatsby';

const Brand = () => (
    <Link to="/"><img className="brand" src={logo} alt="PRHA" /></Link>
)

export default Brand