import { navigate } from "gatsby-link"

const housingApplicationSubmission = values => {
  fetch("/", {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: encode({ "form-name": "housing_application", ...values }),
  })
    .then(() => navigate("/thank-you"))
    .catch(error => alert(error))
}

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&")
}

export default housingApplicationSubmission
