import React from "react"
import { navigate } from "gatsby-link"
import { withFormik, Form, Field } from "formik"
import * as yup from "yup"
import LinkCog from "../linkCog"

const encode = (data) => {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&")
}

const ComplaintsComplimentsForm = ({
  values,
  errors,
  touched,
  isSubmitting,
}) => (
  <Form
    className="form"
    name="complaints_compliments"
    method="post"
    data-netlify="true"
    data-netlify-honeypot="bot-field"
  >
    <input type="hidden" name="form-name" value="complaints_compliments" />
    <div className="form__group form__group--inline form__group--title">
      <label className="form__label" htmlFor="complaints_compliments_title">
        Title
      </label>
      <Field
        id="title"
        component="select"
        name="complaints_compliments_title"
        className="form__select form__select--title"
      >
        <option value="Mr">Mr</option>
        <option value="Mrs">Mrs</option>
        <option value="Miss">Miss</option>
        <option value="Ms">Ms</option>
        <option value="Dr">Dr</option>
      </Field>
      {errors.complaints_compliments_title &&
      touched.complaints_compliments_title ? (
        <p className="form__error">{errors.complaints_compliments_title}</p>
      ) : null}
    </div>
    <div className="form__group form__group--inline form__group--full-name">
      <label className="form__label" htmlFor="complaints_compliments_full_name">
        Full Name
      </label>
      <Field
        type="text"
        className="form__text"
        name="complaints_compliments_full_name"
        id="complaints_compliments_full_name"
      />
      {errors.complaints_compliments_full_name &&
      touched.complaints_compliments_full_name ? (
        <p className="form__error">{errors.complaints_compliments_full_name}</p>
      ) : null}
    </div>

    <div className="form__group form__group--inline form__group--address">
      <label
        className="form__label"
        htmlFor="complaints_compliments_street_name"
      >
        Street Name
      </label>
      <Field
        type="text"
        className="form__text"
        id="complaints_compliments_street_name"
        name="complaints_compliments_street_name"
      />
      {errors.complaints_compliments_street_name &&
      touched.complaints_compliments_street_name ? (
        <p className="form__error">
          {errors.complaints_compliments_street_name}
        </p>
      ) : null}
    </div>
    <div className="form__group form__group--inline form__group--postcode">
      <label className="form__label" htmlFor="complaints_compliments_postcode">
        Postcode
      </label>
      <Field
        type="text"
        className="form__text"
        id="complaints_compliments_postcode"
        name="complaints_compliments_postcode"
      />
      {errors.complaints_compliments_postcode &&
      touched.complaints_compliments_postcode ? (
        <p className="form__error">{errors.complaints_compliments_postcode}</p>
      ) : null}
    </div>
    <div className="form__group form__group--inline form__group--telephone">
      <label className="form__label" htmlFor="complaints_compliments_telephone">
        Daytime Telephone Number
      </label>
      <Field
        type="text"
        className="form__text"
        id="complaints_compliments_telephone"
        name="complaints_compliments_telephone"
      />
      {errors.complaints_compliments_telephone &&
      touched.complaints_compliments_telephone ? (
        <p className="form__error">{errors.complaints_compliments_telephone}</p>
      ) : null}
    </div>
    <div className="form__group form__group--inline form__group--email">
      <label className="form__label" htmlFor="complaints_compliments_email">
        Email Address
      </label>
      <Field
        type="email"
        className="form__text"
        id="complaints_compliments_email"
        name="complaints_compliments_email"
      />
      {errors.complaints_compliments_email &&
      touched.complaints_compliments_email ? (
        <p className="form__error">{errors.complaints_compliments_email}</p>
      ) : null}
    </div>
    <div className="form__group">
      <label className="form__label" htmlFor="complaints_compliments_comments">
        Comments
      </label>
      <p>
        Please include as much detail as you can to enable us to carry out your
        repair as quickly as possible.
      </p>
      <Field
        component="textarea"
        className="form__textarea"
        id="complaints_compliments_comments"
        name="complaints_compliments_comments"
      />
      <p className="form__character-count">0 of 1000 characters</p>
      {errors.complaints_compliments_comments &&
      touched.complaints_compliments_comments ? (
        <p className="form__error">{errors.complaints_compliments_comments}</p>
      ) : null}
    </div>
    <div className="form__group">
      <label
        className="form__label"
        htmlFor="complaints_compliments_process_agreement"
      >
        Data Protection
      </label>
      <p>
        PRHA promises to keep your data safe and secure and in line with the
        General Data Protection Regulation 2018. We will only use your
        information to provide the service you have requested and we will not
        sell your data. You can manage and review your privacy choices at any
        time by contacting You can view <b>PRHA's privacy notice here.</b>
      </p>
      <Field
        className="form__checkbox"
        id="complaints_compliments_process_agreement"
        name="complaints_compliments_process_agreement"
        type="checkbox"
        checked={values.complaints_compliments_process_agreement}
      />
      <label
        className="form__label form__label--checkbox"
        htmlFor="complaints_compliments_process_agreement"
      >
        Do you agree for PRHA to process your information?
      </label>
      {errors.complaints_compliments_process_agreement &&
      touched.complaints_compliments_process_agreement ? (
        <p className="form__error">
          {errors.complaints_compliments_process_agreement}
        </p>
      ) : null}
    </div>
    <button
      className="button button--gold"
      type="submit"
      disabled={isSubmitting}
    >
      Send this form <LinkCog />
    </button>
  </Form>
)

const ComplaintsComplimentsFormik = withFormik({
  mapPropsToValues({
    complaints_compliments_title,
    complaints_compliments_full_name,
    complaints_compliments_street_name,
    complaints_compliments_postcode,
    complaints_compliments_tenant,
    complaints_compliments_telephone,
    complaints_compliments_email,
    complaints_compliments_comments,
    complaints_compliments_process_agreement,
  }) {
    return {
      complaints_compliments_title: "Mr",
      complaints_compliments_full_name: "",
      complaints_compliments_street_name: "",
      complaints_compliments_postcode: "",
      complaints_compliments_tenant: "",
      complaints_compliments_telephone: "",
      complaints_compliments_email: "",
      complaints_compliments_comments: "",
      complaints_compliments_process_agreement: false,
    }
  },
  validationSchema: yup.object().shape({
    complaints_compliments_title: yup.string(),
    complaints_compliments_full_name: yup
      .string()
      .min(3, "Full Name is too short")
      .max(100, "Full Name is too long")
      .required("Full Name is required"),
    complaints_compliments_street_name: yup
      .string()
      .min(3, "Street Name is too short")
      .max(100, "Street Name is too long")
      .required("Street Name is required"),
    complaints_compliments_postcode: yup
      .string()
      .min(5, "Postcode Name is too short")
      .max(10, "Postcode is too long")
      .required("Postcode is required"),
    complaints_compliments_telephone: yup
      .number("This field must be a number")
      .required("Telephone Number is required"),
    complaints_compliments_email: yup
      .string()
      .email("Please enter a valid email address")
      .required("Email Address is required"),
    complaints_compliments_comments: yup
      .string()
      .required("Comments are required"),
    complaints_compliments_process_agreement: yup
      .boolean()
      .oneOf([true], "Please tick to accept the agreement"),
  }),
  handleSubmit(values, { props }) {
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "complaints_compliments", ...values }),
    })
      .then(() => navigate("/thank-you"))
      .catch((error) => alert(error))
  },
})(ComplaintsComplimentsForm)

export default ComplaintsComplimentsFormik
